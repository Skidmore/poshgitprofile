PowerShell script to set poshgit profile settings into your PowerShell Console profile (%userprofile%\Documents\WindowsPowerShell\Microsoft.PowerShell_profile.ps1)
This will import the poshgit PS module and make it available in other PowerShell environments such as the Chef Development Kit.
