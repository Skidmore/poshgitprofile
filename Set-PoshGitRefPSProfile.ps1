<#

.SYNOPSIS
    Adds the Git posh-git Module path to the user profile

.DESCRIPTION
    Add the posh-git module path to the user PowerShell profile so that module is loaded in normal PS Shell.

.NOTES
    Author: Tony Skidmore <anthony.skidmore@accenture.com>
    Date: 17th Feb 2016
    Version: 0.0.2

    Not amending Microsoft.PowerShellISE_profile.ps1 at this time, PowerShell shell host only.
    Only basic checking of exsiting profile for git paths.

#>

try
{
    if(Get-Content $Profile -ErrorAction SilentlyContinue | Select-String -SimpleMatch "posh-git")
    {
        Write-Output "Reference to posh-git already found, not amending $Profile"
        Exit
    }
    else
    {

        if(Test-Path $Profile -ErrorAction Stop)
        {
            $DateStamp = Get-Date -Format "yyyyMMdd-hhmm"
            Copy-Item -Path $Profile -Destination "$Profile.bak-$DateStamp"
            Write-Output "PS Profile modified without error"
        }
        else
        {
            $fileitem = New-Item -Type File $Profile -Force -ErrorAction Stop
        }

        if($ModulePath = (Get-Module -Name posh-git -ErrorAction Stop).Modulebase)
        {

            Write-Output "### posh-git functionlity" | Out-File $Profile -Append

            $Paths = ($env:path -split ";")| Select-String -SimpleMatch "GitHub\PortableGit"

            if($Paths)
            {
                foreach($Path in $Paths)
                {
                    $AddPath = '$env:Path += ";' + $Path +'"'
                    Write-Output $AddPath | Out-File $Profile -Append
                }    
            }
            
            $ModuleName = "posh-git.psm1"
            $FullPath = Join-Path -Path $ModulePath -ChildPath $ModuleName


            Write-Output "Import-Module $FullPath" | Out-File $Profile -Append



            Write-Output "PS Profile modified without error"
        }
        else
        {
            Write-Output "Posh-git module path not found.  Make sure you are running under Git Shell!"
            Exit
        }

    }
}
catch
{
    Write-Error "Unexpected error trying to update PS Profile $($_.Exception)"
}